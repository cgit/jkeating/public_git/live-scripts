#!/bin/sh

if [ -z "$1" ]; then
    echo "need an image"
fi

mount -o loop,ro "$1" /mnt/tmp
mount -o loop,ro -t squashfs /mnt/tmp/LiveOS/squashfs.img /mnt/tmp
mount -o loop,ro /mnt/tmp/LiveOS/ext3fs.img /mnt/tmp

if [ -n "$GET_SHELL" ]; then bash ; fi
rpm --root=/mnt/tmp -qa --qf "%{SIZE}\t%{NAME}\n"
#rpm --root=/mnt/tmp -qa --qf "%{NAME}-%{SIZE}-%{RELEASE}\n"

umount /mnt/tmp
umount /mnt/tmp
umount /mnt/tmp
