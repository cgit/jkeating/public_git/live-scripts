#!/bin/bash

ulimit -c unlimited
export CONFIG=$(readlink -f $1)
export FSLABEL=$2
if [ -z "$LIVEOUTDIR" ]; then
    LIVEOUTDIR="/mnt/firewire/live"
fi
if [ ! -d "$LIVEOUTDIR" ]; then
    mkdir -p $LIVEOUTDIR
fi

clean() {
    exit 0
}

if [ -z $CONFIG -o -z $FSLABEL ]; then
  echo "Need a config and an fslabel"
  exit 1
fi
shift; shift

trap clean SIGINT SIGTERM

thisdir=$(pwd)
if [ -x $thisdir/livecd-creator ]; then
  CREATOR=$thisdir/livecd-creator
else
  CREATOR=/usr/bin/livecd-creator
fi
pushd $LIVEOUTDIR
$CREATOR --config=$CONFIG --fslabel=$FSLABEL --tmpdir=/mnt/firewire/tmp --cache=/mnt/firewire/tmp/cache-$(uname -m) $*
if [ ! -e $FSLABEL.iso ]; then 
  exit 1
fi

date=$(date +%Y%m%d)
outlabel=$(echo $FSLABEL | sed -e "s/-$date//")

for i in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20; do 
    outiso=$LIVEOUTDIR/$outlabel-$date.$i.iso
    if [ ! -e $outiso ]; then break; fi
done
echo "Live image moving to $outiso"
mv $FSLABEL.iso $outiso
chcon -t httpd_sys_content_t $outiso
popd
clean
