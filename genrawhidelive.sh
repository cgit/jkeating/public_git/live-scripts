#!/bin/sh
#
# Generate a full set of rawhide live images

date=-$(date +%Y%m%d)
label="rawhide"

export LIVEOUTDIR=/mnt/firewire/live/$label$date
if [ -z "$CONFIGDIR" ]; then CONFIGDIR=/usr/share/livecd-tools ; fi

i386 sh -x ./mylive.sh $CONFIGDIR/livecd-fedora-desktop.ks $label-i686$date 
#i386 sh ./mylive.sh $CONFIGDIR/livecd-fedora-kde.ks $label-KDE-i686$date 
#sh ./mylive.sh $CONFIGDIR/livecd-fedora-desktop.ks $label-x86_64$date 
#sh ./mylive.sh $CONFIGDIR/livecd-fedora-kde.ks $label-KDE-x86_64$date 
#i386 sh ./mylive.sh $CONFIGDIR/livecd-fedora-electronic-lab.ks $label-FEL-i686$date 
#i386 sh ./mylive.sh $CONFIGDIR/livecd-fedora-developer.ks $label-Developer$date 
#i386 sh ./mylive.sh $CONFIGDIR/livecd-fedora-games.ks $label-games-i686$date 


ls -lh $LIVEOUTDIR
