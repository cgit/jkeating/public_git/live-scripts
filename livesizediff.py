#!/usr/bin/python -tt
# takes two manifest files and compares to find where things grew
# manifests should be created with
#   rpm -qa --qf "%{SIZE}\t%{NAME}\n"
#

import os, sys, string

THRESH = 0.01

def readfile(fn):
    pkgs = {}
    f = open(fn, "r")
    lines = f.readlines()
    f.close()
    for l in lines:
        (size, name) = l.split()
        pkgs[name] = size
    return pkgs

old = sys.argv[1]
new = sys.argv[2]

oldpkgs = readfile(old)
newpkgs = readfile(new)

newlist = []
gonelist = []
growths = []
shrinkage = []

for (pkg, size) in newpkgs.items():
    if not oldpkgs.has_key(pkg):
        newlist.append((pkg, int(size)))
        #print "new package %s: %s" %(pkg, size)
        continue
    oldsize = oldpkgs[pkg]
    if oldsize == "0":
        continue
    if size > oldsize:
        deltapct = (int(size) - int(oldsize)) / float(oldsize)
        if deltapct > THRESH:
            growths.append((pkg, int(size) - int(oldsize), deltapct*100, oldsize, size))
            #print "%s grew by %.2f%% (%s->%s)" %(pkg, deltapct*100, oldsize, size)
    elif size < oldsize:
        deltapct = (int(oldsize) - int(size)) / float(size)
        if deltapct > THRESH:
            shrinkage.append((pkg, int(oldsize) - int(size), deltapct*100, oldsize, size))
            #print "%s shrank by %.2f%% (%s->%s)" %(pkg, deltapct*100, oldsize, size)

for (pkg, size) in oldpkgs.items():
    if not newpkgs.has_key(pkg):
        gonelist.append((pkg, int(size)))

print "old has %d packages" %(len(oldpkgs),)
print "new has %d packages" %(len(newpkgs),)

growths.sort(key=lambda x: -x[1])
shrinkage.sort(key=lambda x: -x[1])
newlist.sort(key=lambda x: -x[1])
gonelist.sort(key=lambda x: -x[1])

print "\nGrowths:"
for growth in growths:
    print "%s grew by %s (%.2f%%) (%s->%s)" % growth

print "\nShrinkage:"
for shrink in shrinkage:
    print "%s shrank by %s (%.2f%%) (%s->%s)" % shrink

print "\nNew packages:"
for new in newlist:
    print "%s: %s" % new

print "\nRemoved packages:"
for gone in gonelist:
    print "%s: %s" % gone
